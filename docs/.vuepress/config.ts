import { defineUserConfig } from 'vuepress'
import type { DefaultThemeOptions } from 'vuepress'

export default defineUserConfig<DefaultThemeOptions>({
    lang: 'zh-cn',
    title: 'Medic Wiki',
    description: '以词库为基础的全新SQ版本',
    base: "/",
    head: [['link', { rel: 'icon', href: '/images/sakura-flower.svg' }]],
    themeConfig: {
        logo: '/images/sakura-flower.svg',
        navbar: [
            {
                text: "指南",
                link: "/guide/introduce"
            },
            {
                text: "参考",
                children: [
                    {
                        text: "Medic",
                        children: [
                            '/reference/api',
                            '/reference/toolkit',
                            '/reference/javascript',
                        ],
                    },
                    {
                        text: '支持库',
                        children: [
                            '/reference/libs/javascript',
                            '/reference/libs/java',
                        ]
                    }
                ]
            },
            {
                text: "参与文档编写",
                link: "https://gitee.com/Miaow233/medic-doc"
            },
{text: "更新日志",link: "/changelog"}
        ],
        sidebar: {
            '/guide/': [
                {
                    text: '指南',
                    children: [
                        '/guide/introduce',
                        '/guide/getting-started',
                        {
                            text: "基础概念",
                            children: [
                                '/guide/api',
                                '/guide/toolkit',
                                '/guide/dic',
                                '/guide/entry',
'/guide/value',
'/guide/constant',
                            ]
                        }
                    ],
                },
            ],
            '/reference/': [
                {
                    text: '参考',
                    children: [
                        '/reference/api',
                        '/reference/toolkit',
                        '/reference/javascript',
                        {
                            text: '支持库',
                            children: [
                                '/reference/libs/javascript',
                                '/reference/libs/java',
                            ]
                        }
                    ],
                }
            ],
        }
    },
})
