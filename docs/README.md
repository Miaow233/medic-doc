---
home: true
title: Medic 使用文档
heroImage: /images/logo.png
actions:
  - text: 了解更多
    link: http://medicxd.top/
    type: secondary
  - text: 快速开始 →
    link: /guide/getting-started.md
    type: primary
features:
  - title: 轻量级
    details: <s>主程序仅有2Mb，</s>极低内存占用
  - title: 易上手
    details: 无需繁杂配置，两行即可实现QQ机器人
  - title: 拓展性
    details: 高性能JS库，原生dex库支持

footer:  Copyright © 2021 Medic Project
---