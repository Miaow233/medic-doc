# JavaScript 支持库
1.4.0版本起，词条支持以JS脚本运行

在相应的关键字下一行标记JS或者javaScript运行JS模式

JavaScript词条交流QQ群：`410351186`

相关语法请参考:[MeJS](http://mejs.ceclin.top:9102)

例:

```js
[词条]
测试
JS
$addText('MEJS消息');
$send();
```

## 更新说明
2.1.0版本更新了JS2，请标记`JS2`来运行新版JS
